import { createFile, sendRequest , testSendRequest } from "./modules/fetchFunc.js";
import { taskQuery} from "./query/tasks.js"
import { serviceObject} from "./query/serviceObject.js"
import { norm } from "./query/norm.js"
import { variables , defaultParams } from "./query/params.js"
import { standard } from "./query/standardProvidings.js"
// import variables from "./query/resource/params.js"
import { airCondDev, airCraftTug, airFieldCab, airStartDev, baggageTractor, deIcingMachine, employee,fireExt,powerSupply,stepLadder,towingCarrier,trap,vacum, waterFill,test, demo ,dev} from "./query/params.js"
import _ from "lodash"
import { resources } from "./query/resources.js";


// testSendRequest(test,serviceObject,variables).then(response => {
//     return response.json();
// }).then( json => {
//         let res = json.data.serviceObjects
//         createFile("serviceObject.json",res);
// })

// testSendRequest(test,norm).then(response => {
//     return response.json();
// }).then( json => {
//         let res = json.data.operationSpecificationNorms
//         createFile("norms.json",res);

// });

testSendRequest(dev,taskQuery,variables).then(response => {
    return response.json();
}).then( json => {
        let res = json.data.tasks
        createFile("tasks.json",res);

});

// testSendRequest(test,standard,defaultParams).then(response => {
//     return response.json();
// }).then( json => {
//         let res = json.data.serviceStandardProvidings
//         createFile("serviceStandartProviding.json",res);

// });
// testSendRequest(demo,resources).then(response => {
//     return response.json();
// }).then( json => {
//         console.log(json)
//         // let res = json.data.resources
//         // createFile("resources.json",res);

// });