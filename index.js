import fs from "fs";
import _  from 'lodash';
import { diffString } from "json-diff";
import { createFile, createLogFile } from "./modules/fetchFunc.js";


let jsonTwo = fs.readFileSync("./json/secondJson.json", "utf-8")
let jsonOne = fs.readFileSync("./json/firstJson.json", "utf-8")

function arraiDifference(arr1, arr2) {
    const diff2 = _.differenceWith(arr2, arr1, _.isEqual);
  
    // if (diff1.length) {
    //   return {
    //     source: "Json1",
    //     data: JSON.stringify(diff1)
    //   };
    // }
  
    if (diff2.length) {
      return {
        source: "Json2",
        data: JSON.stringify(diff2)
      };
    }
  
    return false;
  }
  
function isDataEqual(data1, data2) {
    const { tasks: tasks1, ...restData1 } = data1;
    const { tasks: tasks2, ...restData2 } = data2;
    const tasksDiffs = arraiDifference(data1, data2);
    const isRestDataEqual = _.isEqual(restData1, restData2);

    return {
      tasksDiffs,
      isRestDataEqual
    };
}
  
  var diffEq = isDataEqual(JSON.parse(jsonOne), JSON.parse(jsonTwo));
  console.log(diffEq)
    createFile("diffEq.json",diffEq)

  var resultDiff = diffString(JSON.parse(jsonOne), JSON.parse(jsonTwo));
    console.log(resultDiff)
    createLogFile("resultDiff.log",resultDiff);