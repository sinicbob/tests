describe("Ui testing form login", function () {
    it("Вход на стенд",function ()  {
        cy.visit("http://test.apps.okd.z.rms.pulkovo-airport.com/")

        cy.get('input[type=text]').type('d.andreev');
        cy.get('input[type=password]').type('welcome123');
        cy.get('.is-primary').click();

        cy.get('a[href*="/time-liner"]');
    }),
    it("Переход на таймлайнер ",function ()  {
        cy.get('a[href*="/time-liner"]').click();
    }),
    it("Выход ",function ()  {
        cy.get('.is-large').click();

        cy.get('.is-success').click();
    })
})