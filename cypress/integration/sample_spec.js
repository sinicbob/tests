import {taskQuery} from "../../query/tasks.js"
import {variables} from "../../query/params.js"
import {norm} from "../../query/norm.js"
import {serviceObject} from "../../query/serviceObject.js"

const token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0YXNrX21hbmFnZXIiLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9HUi1TUlRGLVVzZXJBdXRoQ2VydCJ9LHsiYXV0aG9yaXR5IjoiUk9MRV9HQS1KSVJBLVNFUlYifSx7ImF1dGhvcml0eSI6IlJPTEVfdXNlciJ9XSwiaWF0IjoxNjExMjYzNjIwLCJleHAiOjMxMzMzMTY2MjgwMH0.SvlnJHrJ5rB9ohq8bO-BdWjIY9H9sPXeNCAmwtcR2oQ";

describe("Первые тесты", function () {
    it("Проверка статуса запроса задач",function ()  {
        
        cy.request({
            url: `http://api.test.apps.okd.z.rms.pulkovo-airport.com/api/graphql`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token ,
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Connection': 'keep-alive'
            },
            body: JSON.stringify({query: taskQuery, variables:variables}) 
       }).then((response) => { 
               expect(response.status).to.eql(200); 
               expect(response.statusText).to.eql("OK");
               return response;
       }).then((json) => {
            expect(Array.isArray(json.body.data.tasks)).to.eql(true);
        })

    }),
    it("Проверка статуса запроса нормативы", function () {

        cy.request({
            url: `http://api.test.apps.okd.z.rms.pulkovo-airport.com/api/graphql`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token ,
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Connection': 'keep-alive'
            },
            body: JSON.stringify({query: norm, variables:variables}) 
       }).then((response) => { 
               expect(response.status).to.eql(200); 
               expect(response.statusText).to.eql("OK");
       })

    }),``
    it("Проверка статуса запроса рейсов ", function () {

        cy.request({
            url: `http://api.test.apps.okd.z.rms.pulkovo-airport.com/api/graphql`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token ,
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Connection': 'keep-alive'
            },
            body: JSON.stringify({query: serviceObject, variables:variables}) 
       }).then((response) => { 
               expect(response.status).to.eql(200); 
               expect(response.statusText).to.eql("OK");
       }).then((json) => {
        expect(Array.isArray(json.body.data.serviceObjects)).to.eql(true);
       })

    })
})