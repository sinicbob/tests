import { sendRequest , createFile } from "../../modules/fetchFunc.js"
import { taskQuery } from "../../query/tasks.js"
import { demo, variables } from "../../query/params.js"


sendRequest(demo,taskQuery,variables).then(json => {
    let res = json.data.tasks
    createFile("secondJson.json",res);
});