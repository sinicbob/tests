# Auto-tests

# Установка

1.Скачиваем node js
2.Создаем папку для клонирования авто-тестов
3.Открываем в папке терминал в VS code

```bash
Клонируем репозиторий
git clone url
после клонирования пишем команду для перехода в папку с тестами
cd tests
Устанавливаем пакеты
npm install
```

# Создание первого json файла ответа Задач

```javascript
npm run json
```

# Создание второго json файла ответа Задач

```javascript
npm run jsonsec
```

# Запуск сравнения 1-json файла с 2-json файлом (Задач)

```node
 npm start
```

# Создание xlsx файлов

Всех файлов

```javascript
npm run exl
```

Создание excel файлов по отдельности

```javascript
npm run task-exl |Задачи|
npm run norm-exl |Нормативы|
npm run res-exl |Ресурсы|
```

Запуск сравнения справочника "Условия предоставления услуг" и рейсов.
node ./tests/resultDiffStandardProviding.js
