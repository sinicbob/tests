import xl from "excel4node";
import { sendRequest } from "../modules/fetchFunc.js";
import { norm } from "../query/norm.js"
import { demo, test } from "../query/params.js"


var wb = new xl.Workbook();

var ws = wb.addWorksheet('Norms');


const boldStyle = wb.createStyle({
    font: {
      bold: true
    },
    alignment:{
        vertical: 'top'
    }
  });
const wrapStyle = wb.createStyle({
    alignment: {
        vertical: 'top',
        wrapText: true
    }
});
const topStyle = wb.createStyle({
    alignment:{
        vertical: 'top'
    }
});
const errStyle = wb.createStyle({
    alignment:{
        vertical: 'top'
    },
    font:{
        bold: true,
        color: '#ff0000'
    }
});

function createCellName() {
    ws.cell(1,1).string("№").style(boldStyle);
    ws.cell(1,2).string("Наименование").style(boldStyle);
    ws.cell(1,3).string("Время").style(boldStyle);
    ws.cell(1,4).string("Условие").style(boldStyle);
   

    // ws.row(1).filter({
    //     firstRow: 1,
    //     firstColumn: 3,
    //     lastColumn: 3,
    //   });;
    ws.column(1).setWidth(20);
    ws.column(2).setWidth(20);
    ws.column(3).setWidth(20);
    ws.column(3).setWidth(20);
   
}

sendRequest(test,norm).then(json => {
    createCellName()
    
    json.data.operationSpecificationNorms.forEach((item , i) => {
        ws.cell(i+2,1).number(i+1).style(topStyle)
        ws.cell(i+2,2).string(item.operationSpecification.name).style(topStyle)
        if(item.norm == 0 ){
            ws.cell(i+2,3).string(item.norm + " " + item.normUnit).style(errStyle)
        }else {
            ws.cell(i+2,3).string(item.norm + " " + item.normUnit).style(topStyle)
        }
        if (item.conditions.length <= 0) {
            ws.cell(i+2,4).string(JSON.stringify(item.conditions)).style(errStyle);
        } else {
            ws.cell(i+2,4).string(JSON.stringify(item.conditions)).style(topStyle)
        }
       
        
    }); 
    const date = new Date(Date.now())
    wb.write(`./excel/files/norms_${date.toLocaleString().replace(/:/gi,";")}.xlsx`)  ;
    console.log("File xlsx created.")
})

