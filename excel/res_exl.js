import xl from "excel4node";
import { sendRequest } from "../modules/fetchFunc.js";
import { airCondDev, airCraftTug, airFieldCab, airStartDev, baggageTractor, deIcingMachine, employee , fireExt, powerSupply, stepLadder, towingCarrier, trap, vacum, waterFill , demo , test } from "../query/params.js";
import { resource } from "../query/resource/resource.js"


var wb = new xl.Workbook();

const ws = wb.addWorksheet('Employee');
const ws1 = wb.addWorksheet("VacuumClenear");
const ws2 = wb.addWorksheet("Trap");
const ws3 = wb.addWorksheet("StepLadder");
const ws4 = wb.addWorksheet("AirCraftTug");
const ws5 = wb.addWorksheet("PowerSupply");
const ws6 = wb.addWorksheet("TowingCarrier");
const ws7 = wb.addWorksheet("AirStartDev");
const ws8 = wb.addWorksheet("DeIcingMachine");
const ws9 = wb.addWorksheet("FireExt");
const ws10 = wb.addWorksheet("AirFieldCab");
const ws11 = wb.addWorksheet("WaterFill");
const ws12 = wb.addWorksheet("AirCondDev");
const ws13 = wb.addWorksheet("BaggageTractor")


const boldStyle = wb.createStyle({
    font: {
      bold: true
    },
    alignment:{
        vertical: 'top'
    }
  });
const wrapStyle = wb.createStyle({
    alignment: {
        vertical: 'top',
        wrapText: true
    }
});
const topStyle = wb.createStyle({
    alignment:{
        vertical: 'top'
    }
});

function createCellName() {
    ws.cell(1,1).string("№").style(boldStyle);
    ws.cell(1,2).string("mal").style(boldStyle);
    ws.cell(1,3).string("ФИО").style(boldStyle);
    ws.cell(1,4).string("На смене/Не на смене").style(boldStyle);
    ws.cell(1,5).string("").style(boldStyle);
    ws.cell(1,6).string("Код позиции").style(boldStyle);
    ws.cell(1,7).string("Наименование позиции").style(boldStyle);
    ws.cell(1,8).string("").style(boldStyle);
    ws.cell(1,9).string("Время прибытия на смену").style(boldStyle);
    ws.cell(1,10).string("Время ухода со смены").style(boldStyle);
    ws.cell(1,11).string("Skills").style(boldStyle);
   

    // ws.row(1).filter({
    //     firstRow: 1,
    //     firstColumn: 3,
    //     lastColumn: 3,
    //   });;
    ws.column(1).setWidth(6);
    ws.column(2).setWidth(8);
    ws.column(3).setWidth(40);
    ws.column(4).setWidth(22);
    ws.column(5).setWidth(4);
    ws.column(6).setWidth(20);
    ws.column(7).setWidth(80);
    ws.column(8).setWidth(4);
    ws.column(9).setWidth(35);
    ws.column(10).setWidth(35);
    ws.column(11).setWidth(50);
   
}

sendRequest(demo,resource,employee).then(json => {
    createCellName()
    
    json.data.resources.forEach((item , i) => {
        ws.cell(i+2,1).number(i+1).style(topStyle);
        ws.cell(i+2,2).bool(item.resource.malfunction).style(topStyle);
        ws.cell(i+2,3).string(`${item.resource.firstName} ${item.resource.lastName} ${item.resource.middleName} `).style(topStyle);
        ws.cell(i+2,4).string(`${item.resource.currentShiftStatus}`).style(topStyle);
        ws.cell(i+2,5).string("").style(topStyle);
        ws.cell(i+2,6).string(`${item.resource.position.code}`).style(topStyle);
        ws.cell(i+2,7).string(`${item.resource.position.name}`).style(topStyle);
        ws.cell(i+2,8).string("").style(topStyle);
        if(item.resource.shiftJournals.length == 0 ){
            ws.cell(i+2,9).string("?").style(topStyle);
            ws.cell(i+2,10).string("?").style(topStyle);
        }
        else {
            if(item.resource.shiftJournals[0].arrivalDate == undefined){
                ws.cell(i+2,9).string("?").style(topStyle);
            }
            else{
                ws.cell(i+2,9).string(`${item.resource.shiftJournals[0].arrivalDate}`).style(topStyle);
            }
            
            if(item.resource.shiftJournals[0].departureDate == undefined){
                ws.cell(i+2,10).string("?").style(topStyle);
            }
            else{
                ws.cell(i+2,10).string(`${item.resource.shiftJournals[0].departureDate}`).style(topStyle);
            }
        }

        ws.cell(i+2,11).string(`${item.resource.skills}`).style(topStyle);
        
        

    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with Employee")
})

sendRequest(demo,resource,vacum).then(json => {
    ws1.cell(1,1).string("№").style(boldStyle);
    ws1.cell(1,2).string("mal").style(boldStyle);
    ws1.cell(1,3).string("Номер").style(boldStyle);
    ws1.cell(1,4).string("Тип").style(boldStyle);
    ws1.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws1.cell(i+2,1).number(i+1).style(topStyle)
        ws1.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws1.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws1.cell(i+2,4).string(item.resource.vacuumCleanerType).style(topStyle)
        ws1.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with vacuumCleaner")
})


sendRequest(demo,resource,trap).then(json => {
    ws2.cell(1,1).string("№").style(boldStyle);
    ws2.cell(1,2).string("mal").style(boldStyle);
    ws2.cell(1,3).string("Номер").style(boldStyle);
    ws2.cell(1,4).string("Тип").style(boldStyle);

    
    json.data.resources.forEach((item , i) => {
        ws2.cell(i+2,1).number(i+1).style(topStyle)
        ws2.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws2.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws2.cell(i+2,4).string(item.resourceType).style(topStyle)
       
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with trap")
})

sendRequest(demo,resource,stepLadder).then(json => {
    ws3.cell(1,1).string("№").style(boldStyle);
    ws3.cell(1,2).string("mal").style(boldStyle);
    ws3.cell(1,3).string("Номер").style(boldStyle);
    ws3.cell(1,4).string("Тип").style(boldStyle);
    ws3.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws3.cell(i+2,1).number(i+1).style(topStyle)
        ws3.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws3.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws3.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws3.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with stepLadder")
})

sendRequest(demo,resource,airCraftTug).then(json => {
    ws4.cell(1,1).string("№").style(boldStyle);
    ws4.cell(1,2).string("mal").style(boldStyle);
    ws4.cell(1,3).string("Номер").style(boldStyle);
    ws4.cell(1,4).string("Тип").style(boldStyle);
    ws4.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws4.cell(i+2,1).number(i+1).style(topStyle)
        ws4.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws4.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws4.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws4.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with airCraftTug")
})

sendRequest(demo,resource,powerSupply).then(json => {
    ws5.cell(1,1).string("№").style(boldStyle);
    ws5.cell(1,2).string("mal").style(boldStyle);
    ws5.cell(1,3).string("Номер").style(boldStyle);
    ws5.cell(1,4).string("Тип").style(boldStyle);
    ws5.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws5.cell(i+2,1).number(i+1).style(topStyle)
        ws5.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws5.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws5.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws5.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with powerSupply")
})

sendRequest(demo,resource,towingCarrier).then(json => {
    ws6.cell(1,1).string("№").style(boldStyle);
    ws6.cell(1,2).string("mal").style(boldStyle);
    ws6.cell(1,3).string("Номер").style(boldStyle);
    ws6.cell(1,4).string("Тип").style(boldStyle);
    ws6.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws6.cell(i+2,1).number(i+1).style(topStyle)
        ws6.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws6.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws6.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws6.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with towingCarrier")
})

sendRequest(demo,resource,airStartDev).then(json => {
    ws7.cell(1,1).string("№").style(boldStyle);
    ws7.cell(1,2).string("mal").style(boldStyle);
    ws7.cell(1,3).string("Номер").style(boldStyle);
    ws7.cell(1,4).string("Тип").style(boldStyle);
    ws7.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws7.cell(i+2,1).number(i+1).style(topStyle)
        ws7.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws7.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws7.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws7.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with airStartDevice")
})

sendRequest(demo,resource,deIcingMachine).then(json => {
    ws8.cell(1,1).string("№").style(boldStyle);
    ws8.cell(1,2).string("mal").style(boldStyle);
    ws8.cell(1,3).string("Номер").style(boldStyle);
    ws8.cell(1,4).string("Тип").style(boldStyle);
    ws8.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws8.cell(i+2,1).number(i+1).style(topStyle)
        ws8.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws8.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws8.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws8.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with deIcingMachine")
})

sendRequest(demo,resource,fireExt).then(json => {
    ws9.cell(1,1).string("№").style(boldStyle);
    ws9.cell(1,2).string("mal").style(boldStyle);
    ws9.cell(1,3).string("Номер").style(boldStyle);
    ws9.cell(1,4).string("Тип").style(boldStyle);
    ws9.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws9.cell(i+2,1).number(i+1).style(topStyle)
        ws9.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws9.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws9.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws9.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with FireExtinguisher")
})

sendRequest(demo,resource,airFieldCab).then(json => {
    ws10.cell(1,1).string("№").style(boldStyle);
    ws10.cell(1,2).string("mal").style(boldStyle);
    ws10.cell(1,3).string("Номер").style(boldStyle);
    ws10.cell(1,4).string("Тип").style(boldStyle);
    ws10.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws10.cell(i+2,1).number(i+1).style(topStyle)
        ws10.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws10.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws10.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws10.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with AirfieldCabinHeater")
})

sendRequest(demo,resource,waterFill).then(json => {
    ws11.cell(1,1).string("№").style(boldStyle);
    ws11.cell(1,2).string("mal").style(boldStyle);
    ws11.cell(1,3).string("Номер").style(boldStyle);
    ws11.cell(1,4).string("Тип").style(boldStyle);
    ws11.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws11.cell(i+2,1).number(i+1).style(topStyle)
        ws11.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws11.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws11.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws11.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with WaterFillingMachine")
})

sendRequest(demo,resource,airCondDev).then(json => {
    ws12.cell(1,1).string("№").style(boldStyle);
    ws12.cell(1,2).string("mal").style(boldStyle);
    ws12.cell(1,3).string("Номер").style(boldStyle);
    ws12.cell(1,4).string("Тип").style(boldStyle);
    ws12.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws12.cell(i+2,1).number(i+1).style(topStyle)
        ws12.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws12.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws12.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws12.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with AirConditioningDevice")
})

sendRequest(test,resource,baggageTractor).then(json => {
    ws13.cell(1,1).string("№").style(boldStyle);
    ws13.cell(1,2).string("mal").style(boldStyle);
    ws13.cell(1,3).string("Номер").style(boldStyle);
    ws13.cell(1,4).string("Тип").style(boldStyle);
    ws13.cell(1,5).string("Наименование ресурса").style(boldStyle);
    
    json.data.resources.forEach((item , i) => {
        ws13.cell(i+2,1).number(i+1).style(topStyle)
        ws13.cell(i+2,2).bool(item.resource.malfunction).style(topStyle)
        ws13.cell(i+2,3).string(item.resource.resourceNumber).style(topStyle)
        ws13.cell(i+2,4).string(item.resource.resourceNumberType).style(topStyle)
        ws13.cell(i+2,5).string(item.resourceType).style(topStyle)
       
        
    }); 
    wb.write(`./excel/files/resources.xlsx`)  ;
    console.log("Created table with BaggageTractor")
})