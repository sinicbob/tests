import xl from "excel4node";
import { sendRequest } from "../modules/fetchFunc.js";
import { taskQuery } from "../query/tasks.js"
import { test, dev, demo, variables } from "../query/params.js"



var wb = new xl.Workbook();

var ws = wb.addWorksheet('Tasks');

function calcDateTime(date1, date2) {
    return (Date.parse(date2) - Date.parse(date1)) / 1000 / 60
}
const boldStyle = wb.createStyle({
    font: {
      bold: true
    },
    alignment:{
        vertical: 'top'
    }
  });
const wrapStyle = wb.createStyle({
    alignment: {
        vertical: 'top',
        wrapText: true
    }
});
const topStyle = wb.createStyle({
    alignment:{
        vertical: 'top'
    }
});
const errStyle = wb.createStyle({
    alignment:{
        vertical: 'top'
    },
    font:{
        bold: true,
        color: '#ff0000'
    }
})

function createCellName() {
    ws.cell(1,1).string("№").style(boldStyle);
    ws.cell(1,2).string("Статус").style(boldStyle);
    ws.cell(1,3).string("Рейс").style(boldStyle);
    ws.cell(1,4).string("Самолет").style(boldStyle);
    ws.cell(1,5).string("№ стоянки").style(boldStyle);
    ws.cell(1,6).string("Ресурс").style(boldStyle);
    ws.cell(1,7).string("Тип").style(boldStyle);
    ws.cell(1,8).string("ETA").style(boldStyle);
    ws.cell(1,9).string("ETD").style(boldStyle);
    ws.cell(1,10).string("Начало операции").style(boldStyle);
    ws.cell(1,11).string("Окончание операции").style(boldStyle);
    ws.cell(1,12).string("Время").style(boldStyle);
    ws.cell(1,13).string("Наименование операции").style(boldStyle);
    ws.cell(1,14).string("id").style(boldStyle);

    ws.row(1).filter({
        firstRow: 1,
        firstColumn: 2,
        lastColumn: 13,
      });
    
    ws.column(1).setWidth(6.17);
    ws.column(2).setWidth(11);
    ws.column(3).setWidth(9);
    ws.column(4).setWidth(35);
    ws.column(5).setWidth(11);
    ws.column(6).setWidth(21);
    ws.column(7).setWidth(4.8);
    ws.column(8).setWidth(22);
    ws.column(9).setWidth(22);
    ws.column(10).setWidth(22);
    ws.column(11).setWidth(22);
    ws.column(12).setWidth(11);
    ws.column(13).setWidth(40);
    ws.column(14).setWidth(260);
}

sendRequest(dev,taskQuery,variables).then(json => {
    createCellName()
    
    json.data.tasks.forEach((item , i) => {
        ws.cell(i+2,1).number(i+1).style(topStyle)
        ws.cell(i+2,2).string(item.status).style(topStyle)
        ws.cell(i+2,3).string(item.serviceObject.serviceObject.flightNumber).style(topStyle)
        if(item.serviceObject.serviceObject.aircraft == null){
            ws.cell(i+2,4).string("").style(topStyle)
        }
        else if (item.serviceObject.serviceObject.aircraft?.aircraftType?.name == undefined){
            ws.cell(i+2,4).string("").style(topStyle)
        }
        else{
            ws.cell(i+2,4).string(item.serviceObject.serviceObject.aircraft.aircraftType.name).style(topStyle)
        }
       
        
        if(item.serviceObject.serviceObject?.parkingSpot?.number == undefined){
            ws.cell(i+2,5).string("empty")
        }else {
            ws.cell(i+2,5).string(item.serviceObject.serviceObject.parkingSpot.number).style(topStyle)
        }
        
        if(item.resource == undefined){
            ws.cell(i+2,6).string("").style(topStyle)
        }
        else if(item.resource.resourceType == undefined){
            ws.cell(i+2,6).string("?").style(topStyle)
        }
        else {
            ws.cell(i+2,6).string(item.resource.resourceType).style(topStyle)
        }
        
        if (item.resource?.resource == undefined){
            ws.cell(i+2,7).string("");
        }else if (item.resource.resource.vacuumCleanerType == undefined){
            ws.cell(i+2,7).string("?").style(topStyle)
        }else{
            ws.cell(i+2,7).string(item.resource.resource.vacuumCleanerType).style(topStyle)
        }

        if(item.serviceObject.serviceObject.estimatedArrivalDatetime == undefined){
            ws.cell(i+2,9).string(item.serviceObject.serviceObject.estimatedDepartureDatetime).style(topStyle)
        }else {
            ws.cell(i+2,8).string(item.serviceObject.serviceObject.estimatedArrivalDatetime).style(topStyle)
        }
        // ws.cell(i+2,10).string(JSON.stringify(item.operations));
        let startTime = "";
        let endTime = "";
        // let dateTime = "";
        let operationName = "";
        
        item.operations.forEach(( item, j )=> {
            // console.log("item: ", item , " index ", j )
            if (item.scheduledStart == item.scheduledEnd){
                startTime += "(" + (j+1) + ")" + " " + item.scheduledStart + "(-)"  + "\n",
                endTime += "(" + (j+1) + ")" + " " + item.scheduledEnd  + "(-)" + "\n";
            }else {
                startTime += "(" + (j+1) + ")" + " " + item.scheduledStart + "\n";
                endTime += "(" + (j+1) + ")" + " " + item.scheduledEnd + "\n";
            }
            
            //  dateTime += "(" + (j+1) + ")" + " " + calcDateTime(item.scheduledStart, item.scheduledEnd) + " минут" + "\n";
             operationName += "(" + (j+1) + ")" + "" + item.operationSpecification.name + "\n";
            
            ws.cell(i+2,10).string(startTime).style(wrapStyle);
            ws.cell(i+2,11).string(endTime).style(wrapStyle);
            // ws.cell(j+2,12).string(dateTime).style(wrapStyle);
            ws.cell(i+2,13).string(operationName).style(wrapStyle);
        })
        ws.cell(i+2,14).string(item.id).style(wrapStyle);
        
        // item.operationResourceSlots.forEach(item => {
        //     ws.cell(i+2,14).string(JSON.stringify(item.operationResourceRequirements))
        // })
        // wb.write('./excel/files/tasksExcel.xlsx')
        
    }); 
    
    wb.write(`./excel/files/tasksExcelD${variables.from.replace(/:/gi, ";")}-${variables.to.replace(/:/gi, ";")}.xlsx`)  ;
    console.log(`File tasksExcelD${variables.from.replace(/:/gi, ";")}-${variables.to.replace(/:/gi, ";")}.xlsx created. in directory excel/files`)
})

