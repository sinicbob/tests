import { createFile, createLogFile, sendRequest , testSendRequest } from "../modules/fetchFunc.js";
import { readFile } from 'fs/promises';
import { employee, test } from "../query/params.js";
import replaceall from "replaceall"
// const tasks = JSON.parse(await readFile(new URL('../json/tasks.json', import.meta.url)));
const service = JSON.parse(await readFile(new URL('../json/serviceObject.json', import.meta.url)));
const standartProviding = JSON.parse(await readFile(new URL('../json/serviceStandartProviding.json', import.meta.url)));


var values = [];
// функция вывода  условия и значение условия
function objectParamsLog(nameOper,obj){
    let num = Object.keys(obj).length;
    let str = "";
    for(let i = 0; i < num;i++){
        if(Object.keys(obj)[i] == "object" || Object.keys(obj)[i] == "id" || Object.keys(obj)[i] == "type" ){
         
        }else {
          str += `{${Object.keys(obj)[i]}} [${Object.values(obj)[i]}]  `; 
        }
    }
    console.log(`${nameOper} ${str}`);
}
// функция прохождения по массиву со значениями
function arrSerchIn(value,arrL){
    let boo = false;
    arrL.forEach(el => {
        el == value ? boo = true : false
    })
    return boo;
}
// функция прохождения по массиву со значениями
function arrSerchNotIn(value,arrL){
    let boo = true;
    arrL.forEach(el => {
        el == value ? boo = false : true
    })
    return boo;
}

// создание массива со всеми созданными сервисами + проверка на то что условия == применяймым условиям.
function diffArr(num = 0, arr , operation,tsArr,opArr){
    let result = 0;
    arr.forEach(el => {
        (el == true)? result++ : false
    });
    if(result == num){
        tsArr.push({operName:operation,operations:opArr})
    }

}

function propertySerch(str,l,name,){
    let arr = replaceall(".", " ", str).split(" ")
    var result;
    if(l.serviceObjectType == name){
        if(l.serviceObject[arr[0]] != null){
            if(arr.length == 0){
                result = "";
            }
            else if (arr.length == 1){
            result = l.serviceObject[arr[0]]
            }
            else if (arr.length == 2){
                result = l.serviceObject[arr[0]][arr[1]]
            }
            else if (arr.length == 3){
            result = l.serviceObject[arr[0]][arr[1]][arr[2]];
            }
            else if (arr.length == 4){
            result = l.serviceObject[arr[0]][arr[1]][arr[2]][arr[3]];
            }
            return (result == undefined)? "" : result;
        }else {
            return result = "";
        }
    }
   
}



function search(service, obj,number){
        var testArr = [];
    obj.forEach(element => {
        console.log( element.serviceStandard.name ,element.conditions.length)
        let condit = [];
        let errors = [];
        let operationService = [];
        element.serviceStandard.operationSpecifications.forEach(el => {
            operationService.push(el.name);
        })
        // console.log(element);
        element.conditions.forEach(cond => {
            objectParamsLog(cond.object.propertyName ,cond);
            // Переделать все что ниже
            let param = propertySerch(cond.object.propertyName , service , cond.object.objectName);
            if(cond.strCon == "eq"){
                (cond.strCon == "eq" && param == cond.strVal)? condit.push(true) : condit.push(false)
            }
            if(cond.strCon == "neq"){
                (cond.strCon == "neq" && param != cond.strVal )? condit.push(true) : condit.push(false)  
            }
            if(cond.iSCon == "in"){
                (cond.iSCon == "in")? condit.push(arrSerchIn(param, cond.iSVal)) : condit.push(false)
            }
            if(cond.iSCon == "not_in"){
                (cond.iSCon == "not_in")? condit.push(arrSerchNotIn(param, cond.iSVal)) : condit.push(false)
            }
           
            if(cond.emptyOp == "is_not_empty"){
                (cond.emptyOp == "is_not_empty" && param != "" )? condit.push(true) : condit.push(false)
            }
            if(cond.emptyOp == "is_empty"){
                (cond.emptyOp == "is_empty" && param == "" )? condit.push(true) : condit.push(false)
            }
            if(cond.hasOneStrCon == "has_one_of"){
                (cond.hasOneStrCon == "has_one_of")? condit.push(arrSerchIn(param, cond.hasOneStrVal)) : condit.push(false)
            }
            
            // не заполненно:  aircraft.internalStaircase , airline.registeredIn.isoCode ,
            // в рейсах пустое specialEmphasis из-за этого не проходит условие specialEmphasis.code
            // в рейсах пустое значение parkingSpot из-за  этого не проходит условие parkingSpot.number
            // в рейсах пустое значение aircraftType из-за этого не проходит условие aircraftType.iataCode
            
        })
        diffArr(element.conditions.length, condit ,element.serviceStandard.name ,testArr,operationService);
        console.log(`Кол-во: ${condit.length == element.conditions.length}`);
        (condit.length != element.conditions.length) ? errors.push(`${element.serviceStandard.name} ${condit.length == element.conditions.length}`) : "";
        console.log(condit);
        console.log(errors);
        console.log(testArr)
        console.log(operationService);
        
    })
    values.push({name:number,serviceProviding:testArr})

}

service.forEach(element => {
    if(element.serviceObject.airline != null){
        let number = `${element.serviceObject.airline.iataCode}  ${element.serviceObject.flightNumber}`;
        console.log("\n\n\n"+"============================== " + number + " =================================" + "\n\n\n");
        search(element,standartProviding,number);
    }
});
createFile("../json/saveCalculateResult/resultStandardProviding.json",values);