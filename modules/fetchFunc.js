import fetch from "node-fetch"
import fs from 'fs'
import { token } from "../query/params.js"


export function createFile(path,json) {
    fs.open(`./json/${path}`,'w', function(err){
        if(err) throw err
    })
    fs.appendFile(`./json/${path}`,JSON.stringify(json,null,'\t'), 'utf-8' ,(err) => {
        if(err) throw err;
        console.log(`File created name = ${path}`)
    })
}


export function createLogFile(path,log) {
    fs.open(`./logs/${path}`,'w', function(err){
        if(err) throw err
    })
    fs.appendFile(`./logs/${path}`,log, 'utf-8' ,(err) => {
        if(err) throw err;
        console.log(`File created name = ${path}`)
    })
}

export async function sendRequest(url,query,variables){
    return await fetch(url,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token ,
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive'
        },
        body: JSON.stringify({query: query, variables:variables}) 
    })
    .then(response => {
        if (response.status >= 200 && response.status < 300) {  
            return response.json()  
          } else {  
            return Promise.reject(new Error(response.statusText))  
          }  
    })
}

export async function testSendRequest(url,query,variables){
    return await fetch(url,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token ,
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive'
        },
        body: JSON.stringify({query: query, variables:variables}) 
    })
}



