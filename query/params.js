export const token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0YXNrX21hbmFnZXIiLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9HUi1TUlRGLVVzZXJBdXRoQ2VydCJ9LHsiYXV0aG9yaXR5IjoiUk9MRV9HQS1KSVJBLVNFUlYifSx7ImF1dGhvcml0eSI6IlJPTEVfdXNlciJ9XSwiaWF0IjoxNjExMjYzNjIwLCJleHAiOjMxMzMzMTY2MjgwMH0.SvlnJHrJ5rB9ohq8bO-BdWjIY9H9sPXeNCAmwtcR2oQ"

export const test = "http://api.test.apps.okd.z.rms.pulkovo-airport.com/api/graphql";
export const demo = "http://api.demo.apps.mrms.rms.pulkovo-airport.com/api/graphql";
export const dev = "http://api.dev.apps.okd.z.rms.pulkovo-airport.com/api/graphql"

export const variables = {
    from: "2021-07-28T00:00:00",
    to: "2021-07-28T23:59:00"
};
export const nigth = {
    from: "2021-05-05T23:00:00",
    to: "2021-05-06T08:00:00"
};

export const defaultParams = {
    filter: {}
}


export const employee = {mal:false, resourceName:"Employee"};
export const vacum = {mal:false , resourceName:"VacuumCleaner"};
export const trap = {mal: false , resourceName:"Trap"};
export const stepLadder = {mal:false, resourceName:"Stepladder"};
export const airCraftTug = {mal:false, resourceName:"AircraftTug"};
export const powerSupply = {mal:false, resourceName:"PowerSupply"};
export const towingCarrier = {mal:false , resourceName:"TowingCarrier"};
export const airStartDev = {mal:false, resourceName:"AirStartDevice"};
export const deIcingMachine = {mal:false, resourceName:"DeIcingMachine"};
export const fireExt = {mal:false, resourceName:"FireExtinguisher"};
export const airFieldCab = {mal:false, resourceName:"AirfieldCabinHeater"};
export const waterFill = {mal:false, resourceName:"WaterFillingMachine"};
export const airCondDev = {mal:false, resourceName:"AirConditioningDevice"};
export const baggageTractor = {mal:false, resourceName:"BaggageTractor"};


