export const norm = `
{
    operationSpecificationNorms{
      id
     norm
     normUnit
      operationSpecification{
        name
        arrivalPlaceExecutionTimeRequired
        departurePlaceExecutionTimeRequired
        startAndFinishOperationTimeRequired
      }
      conditions{
        __typename 
         ... on ComparableIntCondition {
             operator
             value
         }
         ... on EqualityStringCondition{
             operator2 : operator
             value2 : value
         }
         
       }
      }
 }`