export const serviceObject = `
query getServiceObjects(
    $from: DateTime,
    $to: DateTime
) {
    serviceObjects(
        filter: {
            queryFrom: $from,
            queryTo: $to
        }
    ) {
        serviceObject {
            ... on Flight {
                actualArrivalDatetime
                actualDepartureDatetime
                flightSpecification{
                  arrivalAirport{
                  id
                  iataCode
                  }
                  departureAirport{
                    id
                    iataCode
                  }
                }
                aircraftType {
                    height
                    iataCode
                    icaoCode
                    id
                    length
                    name
                }
                airline {
                    iataCode
                    icaoCode
                    id
                    name
                    registeredIn {
                        isoCode
                        name
                    }
                }
                cancellationDatetime
                estimatedArrivalDatetime
                estimatedDepartureDatetime
                flightNumber
                id
                parkingSpotScheduledEndDatetime
                parkingSpotScheduledStartDatetime
                scheduledDepartureDatetime
                aircraft {
                    aircraftType {
                        height
                        iataCode
                        icaoCode
                        id
                        length
                        name
                    }
                    id
                    internalLadder
                    seatsBusinessClassQty
                    seatsEconomClassQty
                    seatsFirstClassQty
                    seatsQty
                    tailNumber
                }
                linkedFlight{
                    trafficType
                }
                parkingSpot{
                    number
                    platform
                    type
                }
                serviceType{
                  code
                }
                specialEmphasis{
                    code
                }
                trafficType
            }
        }
        serviceObjectType
    }
}`