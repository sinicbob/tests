export const resources = ` resources(filter: {malfunction: false}){
  resource{
     ... on Trap {
         malfunction
         resourceNumber
       }
       ... on Bus { 
         malfunction
          busType
       }
       ... on Employee { 
         malfunction
          firstName
          middleName
          lastName
       }
       ... on VacuumCleaner {
         malfunction
          resourceNumber
          vacuumCleanerType
       }
       ... on Stepladder {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on AircraftTug {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on PowerSupply {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on TowingCarrier {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on AirStartDevice {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on DeIcingMachine {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on FireExtinguisher {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on AirfieldCabinHeater {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on WaterFillingMachine{
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on WasteDisposalMachine {
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on AirConditioningDevice{
         malfunction
         resourceNumber
         resourceNumberType
       }
       ... on BaggageTractor {
         malfunction
         resourceNumber
         resourceNumberType
       }
  }
  resourceType
} `