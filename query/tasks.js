export const taskQuery = `
query TaskNow($from: DateTime, $to: DateTime){
    tasks(filter: {queryFrom:$from , queryTo:$to}){
     id 
     status 
    availableTaskStatusTransitions {
      direction
      newStatus
    }
    actualArrivalToPlaceExecution
    actualDepartureToPlaceExecution
    arrivalPlaceExecutionTimeRequired
    departurePlaceExecutionTimeRequired
    serviceObject {
      serviceObjectType
      serviceObject {
        ... on Aircraft {
          tailNumber
          aircraftType {
            name
          }
        }
        ... on Flight {
          flightNumber
          aircraft {
            tailNumber
            aircraftType {
              name
            }
          }
          parkingSpot {
            number
            platform
            type
          }
          estimatedDepartureDatetime
          estimatedArrivalDatetime
        }
      }
    }
    resource {
      resource { 
        ... on Bus { 
          busType
        }
        ... on Employee { 
          firstName
          middleName
          lastName
        }
        ... on VacuumCleaner { 
          resourceNumber
          vacuumCleanerType
        }
      }
      resourceType 
    }
    operations {
      actualEnd
      actualStart
      scheduledEnd
      scheduledStart
      operationSpecification {
        name
        description
      }
    }
    
  }
}`

