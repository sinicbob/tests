export const resource = `
query getResource($mal: Boolean, $resourceName: ResourceType){
    resources(filter: {malfunction: $mal, resourceType:$resourceName}){
     resource{
        ... on Trap {
            ...mal
            resourceNumber
          }
          ... on Bus { 
            ...mal
             busType
          }
          ... on Employee { 
            ...mal
             firstName
             middleName
             lastName
             currentShiftStatus
             position {
              code
              name
            }
            shiftJournals {
              arrivalDate
              departureDate
            }
            shifts {
              datePlanBegin
              datePlanEnd
              id
              status
            }
          skills
          subdivision {
              id
              name
              hrmId
            }
          }
          ... on VacuumCleaner {
            ...mal
             resourceNumber
             vacuumCleanerType
          }
          ... on Stepladder {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on AircraftTug {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on PowerSupply {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on TowingCarrier {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on AirStartDevice {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on DeIcingMachine {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on FireExtinguisher {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on AirfieldCabinHeater {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on WaterFillingMachine{
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on WasteDisposalMachine {
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on AirConditioningDevice{
            ...mal
            resourceNumber
            resourceNumberType
          }
          ... on BaggageTractor {
            ...mal
            resourceNumber
            resourceNumberType
          }
     }
     resourceType
   } 
   }
   fragment mal on IResource{
     malfunction
     operationGroup
   }`;