export const standard = `
query serviceStandardProvidings($filter: ServiceStandardProvidingFilter) {
  serviceStandardProvidings(filter: $filter) {
    conditions {
      ... on ComparableFloatCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        fcOp: operator
        type
        fcVal: value
      }
      ... on ComparableIntCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        icOp: operator
        type
        icVal: value
      }
      ... on ContainsCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        conConOp: operator
        type
        conConVal: value {
          objectName
          propertyName
          propertyType
        }
      }
      ... on EqualityBooleanCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        boolCon: operator
        type
        boolVal: value
      }
      ... on EqualityFloatCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        flCon: operator
        type
        flVal: value
      }
      ... on EqualityIntCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        eqCon: operator
        type
        eqVal: value
      }
      ... on EqualityStringCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        strCon: operator
        type
        strVal: value
      }
      ... on HasAllOfIntCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        allConOp: operator
        type
        allValOp: value
      }
      ... on HasAllOfStringCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        allOfConOp: operator
        type
        allOfConVal: value
      }
      ... on HasOneOfIntCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        hasOneOp: operator
        type
        hasOneVal: value
      }
      ... on HasOneOfStringCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        hasOneStrCon: operator
        type
        hasOneStrVal: value
      }
      ... on InIntCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        iiCon: operator
        type
        iiVal: value
      }
      ... on InStringCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        iSCon: operator
        type
        iSVal: value
      }
      ... on EmptyValueCondition {
        id
        object {
          objectName
          propertyName
          propertyType
        }
        emptyOp: operator
        emptyType: type
      }
    }
    id
    onDemand
    serviceStandard {
      erpId
      id
      name
      operationSpecifications {
        arrivalPlaceExecutionTimeRequired
        departurePlaceExecutionTimeRequired
        description
        id
        name
        serviceStandard {
          erpId
          id
          name
          responsibleSubdivision {
            hrmId
            id
            name
          }
          type
        }
        startAndFinishOperationTimeRequired
      }
      type
    }
  }
}`;