import http from "http"
import { readFile } from 'fs/promises';
const json = JSON.parse(await readFile(new URL('../json/tasks.json', import.meta.url)));

const html = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test document</title>
    <script type="module" src="./json/tasks.json"></script>

</head>
<body>
    

    <script type="module" src="./index.js"></script>
    <script type="module" src="./modules/fetchFunc.js"></script>
    <script type="module" src="./test.js"></script>
</body>
</html>`



http.createServer((req,res) => {
    switch(req.url){
        case '/':
            res.writeHead(200,{'Content-Type': 'text/html'})
            res.end(html);
        case "/json":
            res.writeHead(200, {'Content-Type': 'application/json'})
            res.end(json)
    }
}).listen(3000, () => console.log("Server working"));